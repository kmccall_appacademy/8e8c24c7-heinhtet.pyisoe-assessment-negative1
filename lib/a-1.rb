# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  fullnums = (nums[0]..nums[-1]).to_a
  fullnums.reject! {|num| nums.include?(num)}
  fullnums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  stringarr = binary.to_s.split('')
  sumarray = []

  stringarr.reverse.each_with_index do |stringel, idx|
    # p "#{stringel.to_i} * (2 ** #{idx})"
    sumarray << stringel.to_i * (2 ** idx)

  end

  sumarray.reduce(:+)
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    new_hsh = {}
    self.each do |k,v|
      if (prc.call(k,v))
      new_hsh[k] = v
      end
    end
    new_hsh
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    new_hsh = {}
    newkeys = (self.keys + hash.keys).uniq
    commonkeys = self.keys.select {|el| hash.keys.include?(el)}

    newkeys.each do |newkey|
      if commonkeys.include?(newkey)
        if block_given?
        new_hsh[newkey] = yield(newkey, self.fetch(newkey), hash.fetch(newkey))
        else
        new_hsh[newkey] = hash.fetch(newkey)
        end
      elsif self.keys.include?(newkey)
        new_hsh[newkey] = self.fetch(newkey)
      else
        new_hsh[newkey] = hash.fetch(newkey)
      end
    end

  new_hsh
  end
end
# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  lucas_hsh = {0 => 2}
  new_indices = (0..n).to_a
  neg_indices = (n..0).to_a
  new_vals = [2,1]
  neg_vals = [2,-1]

    #generate positive lucas nums
    if n > 0

        new_indices.each_with_index do |el, idx|
            next if idx == 0 || idx == 1
            previous = new_vals[idx-1]
            previous2 = new_vals[idx-2]
            # puts "prev1: #{previous} + prev2: #{previous2} = new val: #{previous + previous2}"
            new_vals << previous + previous2

            #put them together in hash
          new_indices.each_with_index do |key, idx1|
            new_vals.each_with_index do |val, idx2|
                if idx1 == idx2
                lucas_hsh[key] = val
                end
            end
          end
     end

    elsif n < 0
      #generate negative lucas numbers
      neg_indices.reverse.each_with_index do |el, idx|
          next if idx == 0 || idx == 1
          previous = neg_vals[idx-1]
          previous2 = neg_vals[idx-2]
          puts "prev2: #{previous2} + prev: #{previous} = new val: #{previous2 - previous}"
          neg_vals << previous2 - previous
      end

      #put them together in hash
        neg_indices.reverse.each_with_index do |key, idx1|
          neg_vals.each_with_index do |val, idx2|
              if idx1 == idx2
              lucas_hsh[key] = val
              end
          end
        end
      end
      p neg_vals
      lucas_hsh.fetch(n)
end


# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  arr = string.split('')
  front = false
  dromes = []
  #front to back
  string.size.times do
    joined = arr.join('')
    if palindrome?(joined)
      dromes << joined if joined.length > 1
    else
      arr.pop if front
      arr.shift if !front
      front = !front
    end

    #back to front
    string.size.times do
      joined = arr.join('')
        if palindrome?(joined)
          dromes << joined if joined.length > 1
        else
          arr.pop if !front
          arr.shift if front
          front = !front
        end
    end
  end

  if dromes.empty? || dromes.all? {|el| el.to_s.length <= 2}
    false
  else
    dromes.sort_by {|el| el.to_s.length}.max.length
  end

end

def palindrome?(str)
  str == str.reverse
end
